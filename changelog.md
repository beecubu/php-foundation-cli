1.4.0
-----
- Afegides noves classes per crear aplicacions CLI
  - Application: Punt d'inici de l'aplicació que mostra Vistes.
  - ViewController: Les pantalles a mostrar.
- Afegit un nou mètode "getIntValue" per obtenir valors enters.
- Els mètodes "title" i "section" ara poden ser multilínia.

1.3.0
-----
- Actualitzat el composer.

1.2.0
-----
- Afegit un nou mètode "getDateTime" per obtenir dates amb una hora concreta.
- El "getDate" ara retorna les hores i munuts a 0.

1.1.1
-----
- Els títols i seccions ara fan servir l'ascii "línia" i "doble-línia".

1.1.0
-----
- Afegida la possibilitat de dibuixar barres de percentatges.

1.0.1
-----
- Actualitzades les dependencies (Helpers).

1.0.0
-----
- Versió inicial.
