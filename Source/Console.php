<?php

namespace Beecubu\Foundation\Cli;

use DateTime;
use function Beecubu\Foundation\Helpers\DateTime\compareDates;
use function Beecubu\Foundation\Helpers\String\mb_str_pad;

/**
 * Funcions bàsiques per crear aplicacions CLI.
 */
class Console
{
    private static $readlineInstalled = false;

    private static $colors = [
        'names' => [
            // foreground colors
            'default'      => '0;0',
            'black'        => '0;30',
            'blue'         => '0;34',
            'green'        => '0;32',
            'cyan'         => '0;36',
            'red'          => '0;31',
            'purple'       => '0;35',
            'brown'        => '0;33',
            'yellow'       => '0;33',
            'white'        => '0;37',
            // background colors
            'b_black'      => '40',
            'b_red'        => '41',
            'b_green'      => '42',
            'b_yellow'     => '43',
            'b_blue'       => '44',
            'b_magenta'    => '45',
            'b_cyan'       => '46',
            'b_light_gray' => '47',
            'b_default'    => '49',
        ],
        'tags'  => [],
        'codes' => [],
    ];

    /**
     * Inicialitza la llista de colors.
     */
    private static function initColors(): void
    {
        if (count(static::$colors['tags']) == 0)
        {
            foreach (static::$colors['names'] as $color => $value)
            {
                static::$colors['tags'][] = "/<$color>/";
                static::$colors['codes'][] = "\033[{$value}m";
            }
        }
    }

    /**
     * Converteix els colors html en colors de consola.
     *
     * @param string $color El color html q convertir.
     *
     * @return string El color en format consola.
     */
    private static function colorToCli(string $color): string
    {
        static::initColors();
        // convert color to cli code color
        if (array_key_exists($color, static::$colors['names']))
        {
            return "\033[".static::$colors['names'][$color]."m";
        }
        return '';
    }

    /** @ignore */
    public function __construct()
    {
        self::$readlineInstalled = function_exists('readline');
    }

    /** @ignore */
    public function __destruct()
    {
        // restore default console color
        fwrite(STDOUT, static::colorToCli("default"));
    }

    /**
     * Pinta una línia a la consola amb coloraines! :)
     *
     * @param string $msg Text a mostrar.
     * @param bool $newLine Si es vol fer un salt de línia al final o no.
     */
    public static function line(string $msg = '', bool $newLine = true): void
    {
        static::initColors();
        // no message to display?
        if ($msg === '' and $newLine)
        {
            fwrite(STDOUT, PHP_EOL);
            return;
        }
        // open colors
        $msg = preg_replace(static::$colors['tags'], static::$colors['codes'], $msg);
        // close colors and others
        foreach (static::$colors['names'] as $color => $value)
        {
            if (strpos($color, 'b_') === 0) // background color
                $msg = str_replace("</$color>", static::colorToCli('b_default'), $msg);
            else // font color
                $msg = str_replace("</$color>", static::colorToCli('default'), $msg);
        }
        // print the final line
        $msg .= static::colorToCli('b_default').static::colorToCli('default');
        // add a new line?
        if ($newLine) $msg .= PHP_EOL;
        // print line
        fwrite(STDOUT, $msg);
    }

    /**
     * Pinta un grup de línies a la consola amb coloraines! :)
     *
     * @param array $lines Les línies a mostrar.
     */
    public static function lines(array $lines): void
    {
        foreach ($lines as $line)
        {
            static::line($line);
        }
    }

    /**
     * Pinta per pantalla un títol (també neteja la pantalla).
     *
     * @param string $title Títol de la secció.
     * @param bool $clean TRUE = neteja la pantalla, FALSE = no.
     */
    public static function title(string $title, bool $clean = true): void
    {
        if ($clean) Console::clear();
        Console::line();
        foreach (explode(PHP_EOL, $title) as $line)
        {
            Console::center($line);
        }
        Console::separator('═');
        Console::line();
    }

    /**
     * Crea una nova secció (és semblant a un títol, però amb separadors als dos llocs i sense netejar la pantalla).
     *
     * @param string $title Text a mostrar.
     */
    public static function section(string $title): void
    {
        Console::line();
        Console::separator('═');
        foreach (explode(PHP_EOL, $title) as $line)
        {
            Console::center($line);
        }
        Console::separator('═');
        Console::line();
    }

    /**
     * Pinta un text centrat al mig.
     *
     * @param mixed $msg Text a centrar.
     * @param int $length Referencia d'¡'on centrar.
     * @param string $color Color de la línia.
     */
    public static function center(string $msg, int $length = 78, string $color = 'cyan'): void
    {
        Console::line("<$color>".mb_str_pad($msg, $length, ' ', STR_PAD_BOTH));
    }

    /**
     * Pinta un separador a la pantalla.
     *
     * @param string $lineStyle Estil de la línia
     * @param int $length Longitud de la línia (per defecte 78).
     */
    public static function separator(string $lineStyle = '─', int $length = 78): void
    {
        Console::line('<cyan>'.mb_str_pad('', $length, $lineStyle));
    }

    /**
     * Pinta un missatge d'error.
     *
     * @param string $msg El text a mostrar.
     * @param bool $enterToContinue Enter per continuar.
     */
    public static function error(string $msg, bool $enterToContinue = false): void
    {
        Console::line();
        Console::line("<b_red>Error: $msg");
        Console::line();
        // enter to continue?
        if ($enterToContinue) Console::input();
    }

    /**
     * Pinta un missatge d'ok.
     *
     * @param string $msg El text a mostrar.
     * @param bool $enterToContinue Enter per continuar.
     */
    public static function success(string $msg, bool $enterToContinue = false): void
    {
        Console::line();
        Console::line("<green>$msg");
        Console::line();
        // enter to continue?
        if ($enterToContinue) Console::input();
    }

    /**
     * Pinta un missatge d'atenció.
     *
     * @param mixed $msg Description.
     * @param bool $enterToContinue Enter per continuar.
     */
    public static function warning(string $msg, bool $enterToContinue = false): void
    {
        Console::line();
        Console::line("<yellow>Warning: $msg");
        Console::line();
        // enter to continue?
        if ($enterToContinue) Console::input();
    }

    /**
     * Neteja la pantalla
     */
    public static function clear(): void
    {
        fwrite(STDOUT, exec('clear'));
    }

    /**
     * Pinta un progressbar per pantalla.
     *
     * @param int $value Si $init es TRUE llavors és el total, si l'$init es false llavors és l'step...
     * @param bool $init TRUE = És mode configuració, FALSE = mode print on
     * @param string $caption NULL = El text per defecte.
     */
    public static function progressbar(int $value = 1, bool $init = false, ?string $caption = null): void
    {
        static $total, $done;

        if ($init)
        {
            $total = $value;
            $done = 0;
        }
        else // print progressbar
        {
            $done += $value;
        }
        // update progress
        $progress = floor(($done/$total)*58);
        // progress to display
        $progressStr = str_pad(floor(($done/$total)*100), 3, ' ', STR_PAD_LEFT);
        // show progressbar
        $left = 58 - $progress;
        fwrite(STDOUT, "\033[0G\033[2K".str_repeat('▓', $progress).str_repeat('░', $left)."  $progressStr% $caption");
    }

    /**
     * Mostra un spinner per als progressbar que no sabem la duració
     *
     * @param string $text El text a mostrar, si el text es false llavors esborra tot...
     * @param int $style L'estil de l'spinner
     */
    public static function spinner(string $text = '', int $style = 3): void
    {
        static $step = 0;
        static $lastUpdate;

        $styles = [
            1 => ['speed' => 0.04, 'frames' => ['-', '\\', '-', '|', '/', '-']],
            2 => ['speed' => 0.04, 'frames' => ['▁', '▂', '▃', '▄', '▅', '▆', '▇', '█', '▇', '▆', '▅', '▄', '▃', '▂']],
            3 => ['speed' => 0.04, 'frames' => ['⠋', '⠙', '⠹', '⠸', '⠼', '⠴', '⠦', '⠧', '⠇', '⠏']],
            4 => ['speed' => 0.05, 'frames' => ['⢄', '⢂', '⢁', '⡁', '⡈', '⡐', '⡠']],
            5 => ['speed' => 0.10, 'frames' => ['⊙⊙', '◡◡', '⊙⊙', '◠◠']],
            6 => ['speed' => 0.08, 'frames' => ['🌑', '🌒', '🌓', '🌔', '🌕', '🌝', '🌖', '🌗', '🌘']],
        ];

        // is the spinner active?
        if ($text !== false)
        {
            $time = microtime(true);
            $diff = $time - $lastUpdate;
            // ignore this update
            if ($diff < $styles[$style]['speed']) return;
            // ok, remember this
            $lastUpdate = $time;
        }

        // cleanup the previous spinner
        fwrite(STDOUT, "\e[0G\e[2K");

        if ($text !== false && $style > 0 && $style < count($styles) + 1)
        {
            fwrite(STDOUT, $styles[$style]['frames'][$step]." ".$text);
            // iteration control
            $step++;
            if ($step === count($styles[$style]['frames'])) $step = 0;
        }
    }

    /**
     * Permet llegir el text teclejat
     *
     * @return string El text teclejat
     */
    public static function input(): string
    {
        if (self::$readlineInstalled)
        {
            $input = trim(readline(static::colorToCli('green')));
        }
        else // fgets STDIN version
        {
            fwrite(STDOUT, static::colorToCli('green'));
            // read user input
            $input = trim(fgets(STDIN));
        }
        // reset any configuration
        fwrite(STDOUT, static::colorToCli('default'));
        // the input text
        return $input;
    }

    /**
     * Pregunta a l'usuari quina opció vol i no deixa continuar fins que s'esculli una
     *
     * @param mixed , ... El primer paràmetre és el títol, Totes les opcions que es permetin
     *
     * @return array Array amb els valors falsos i un únic true (el de la opció seleccionada)
     */
    public static function getOption(): array
    {
        $array = func_get_args();
        // el primer paràmetre és el missatge
        if (count($array) > 1)
        {
            $msg = $array[0];
            // eliminem el primer paràmetre
            unset($array[0]);
            $array = array_values($array);
            // get option
            do
            {
                // demanem a l'usuari que escrigui
                $input = static::getString($msg.' ['.implode('|', $array).']', false);
                // comprovem que s'ha seleccionat alguna de les opcions
                if (in_array($input, $array))
                {
                    foreach ($array as $key => $value)
                    {
                        $array[$key] = $value === $input;
                    }
                    return $array;
                }
                else // ai ai ai...
                {
                    static::error('No has seleccionat cap de les opcions disponibles...');
                }
            }
            while (true);
        }
        return [];
    }

    /**
     * Pregunta a l'usuari quina opció vol i no deixa continuar fins que s'esculli una
     *
     * @param mixed , ... El primer paràmetre és el títol, Totes les opcions que es permetin
     *
     * @return int Retorna l'index de la opció seleccionada
     */
    public static function getOptionIndex(): int
    {
        $array = func_get_args();
        // el primer paràmetre és el missatge
        if (count($array) > 1)
        {
            $msg = $array[0];
            // eliminem el primer paràmetre
            unset($array[0]);
            $array = array_values($array);
            // get option
            do
            {
                // demanem a l'usuari que escrigui
                $input = static::getString($msg.' ['.implode('|', $array).']', false);
                // comprovem que s'ha seleccionat alguna de les opcions
                if (in_array($input, $array))
                {
                    return array_search($input, $array);
                }
                else // ai ai ai...
                {
                    static::error('No has seleccionat cap de les opcions disponibles...');
                }
            }
            while (true);
        }
        return 0;
    }

    /**
     * Demana al usuari un text
     *
     * @param string $msg Text a mostrar.
     * @param bool $canBeEmpty TRUE = L'usuari pot deixar buit el camp, FALSE = no.
     * @param string $default El text a mostrar per defecte.
     *
     * @return string El text entrat per l'usuari.
     */
    public static function getString(string $msg, bool $canBeEmpty = true, ?string $default = null): string
    {
        // l'usuari pot passar?
        if ($canBeEmpty) $msg .= ' (<brown>enter</brown> per passar): ';
        else $msg .= ': ';
        // mostrem el text per defecte
        if ($default) $msg .= '<cyan>'.$default.'</cyan> ';
        // demanem el text
        do
        {
            static::line(" » ".$msg, false);
            // demanem el text
            $value = trim(static::input());
            // set the default value if no text is provided
            if ($default and $value == '') $value = $default;
            // finished?
            if ($canBeEmpty) return $value;
            // comprovem que no estigui buit
            if ($value == '')
            {
                static::error('Aquest camp és obligatori!');
            }
            else // ok, finish
            {
                return $value;
            }
        }
        while (true);
        // should never reach this return, if happens PHP will throw an exception
        return '';
    }

    /**
     * Demana al usuari un valor entre dos números (opcional)
     *
     * @param string $msg Text a mostrar.
     * @param float|null $min Valor minim (FALSE = no hi ha minim).
     * @param float|null $max Valor maxim (FALSE = no hi ha maxim).
     * @param bool $showMinMax Si es volen mostrar el min i max (TRUE per defecte)
     * @param float|null $default El valor per defecte.
     *
     * @return float El número que ha introduït l'usuari.
     */
    public static function getValue(string $msg, ?float $min = null, ?float $max = null, bool $showMinMax = true, ?float $default = null): float
    {
        // tenim un minim?
        $minMax = [];
        // mostrem el min i max?
        if ($showMinMax)
        {
            if ($min !== false) $minMax[] = 'min = '.(float)$min;
            if ($max !== false) $minMax[] = 'max = '.(float)$max;
        }
        // build the info
        $info = implode(', ', $minMax);
        // has info?
        if ($info != '') $info = " ($info)";
        // demanem el valor
        do
        {
            // print the message
            $value = (float)str_replace(',', '.', static::getString($msg.$info, false, $default));
            // validem números
            if ($min !== null)
            {
                if ($value < (float)$min)
                {
                    static::error("El valor '$value' és massa petit.");
                    continue;
                }
            }
            if ($max !== null)
            {
                if ($value > (float)$max)
                {
                    static::error("El valor '$value' és massa gran.");
                    continue;
                }
            }
            return $value;
        }
        while (true);
    }

    /**
     * Demana al usuari un valor entre dos números enters (opcional)
     *
     * @param string $msg Text a mostrar.
     * @param float|null $min Valor minim (FALSE = no hi ha minim).
     * @param float|null $max Valor maxim (FALSE = no hi ha maxim).
     * @param bool $showMinMax Si es volen mostrar el min i max (TRUE per defecte)
     * @param float|null $default El valor per defecte.
     *
     * @return int
     */
    public static function getIntValue(string $msg, ?float $min = null, ?float $max = null, bool $showMinMax = true, ?float $default = null): int
    {
        return (int)static::getValue($msg, $min, $max, $showMinMax, $default);
    }

    /**
     * Demana al usuari que respongui SI o NO.
     *
     * @param string $msg El missatge a mostrar.
     *
     * @return bool TRUE = Yes, FALSE = no.
     */
    public static function getYesNo(string $msg): bool
    {
        $values = self::getOption($msg, 's', 'n');
        // return the user selection
        return $values[0];
    }

    /**
     * Demana al usuari una data entre dos dates (opcional)
     *
     * @param string $msg Text a mostrar.
     * @param DateTime|null $min Data mínima (FALSE = no hi ha minim).
     * @param DateTime|null $max Data màxima (FALSE = no hi ha maxim).
     * @param bool $canBeEmpty TRUE = L'usuari pot passar, FALSE = no.
     * @param DateTime|null $default La data a posar per defecte.
     *
     * @return DateTime La data que ha introduït l'usuari.
     */
    public static function getDate(string $msg, ?DateTime $min = null, ?DateTime $max = null, bool $canBeEmpty = true, ?DateTime $default = null): ?DateTime
    {
        // tenim un minim?
        $minMax = [];
        if ($min !== null) $minMax[] = 'min = '.$min->format('d/m/Y');
        if ($max !== null) $minMax[] = 'max = '.$max->format('d/m/Y');
        // build the info
        $info = implode(', ', $minMax);
        // has info?
        if ($info != '') $info = " ($info)";
        // demanem la data
        do
        {
            $value = static::getString($msg.$info, $canBeEmpty, $default instanceof DateTime ? $default->format('d/m/Y') : null);
            // check for empty values
            if ($canBeEmpty and $value == '')
            {
                return null;
            }
            // print the message
            $value = DateTime::createFromFormat('d/m/Y', $value);
            // comprovem que tenim una data valida
            if ( ! $value instanceof DateTime)
            {
                static::error("No has introduït una data vàlida.");
                continue;
            }
            // validem les dates
            if ($min !== null)
            {
                if (compareDates($value, $min) < 0)
                {
                    static::error("La data '".$value->format('d/m/Y')."' és massa vella.");
                    continue;
                }
            }
            if ($max !== null)
            {
                if (compareDates($value, $max) > 0)
                {
                    static::error("La data '".$value->format('d/m/Y')."' és massa futura.");
                    continue;
                }
            }
            $value->setTime(0,0);
            return $value;
        }
        while (true);
    }

    /**
     * Demana al usuari una data entre dos dates (opcional)
     *
     * @param string $msg Text a mostrar.
     * @param DateTime|null $min Data mínima (FALSE = no hi ha minim).
     * @param DateTime|null $max Data màxima (FALSE = no hi ha maxim).
     * @param bool $canBeEmpty TRUE = L'usuari pot passar, FALSE = no.
     * @param DateTime|null $default La data a posar per defecte.
     *
     * @return DateTime La data que ha introduït l'usuari.
     */
    public static function getDateTime(string $msg, ?DateTime $min = null, ?DateTime $max = null, bool $canBeEmpty = true, ?DateTime $default = null): ?DateTime
    {
        // tenim un minim?
        $minMax = [];
        if ($min !== null) $minMax[] = 'min = '.$min->format('d/m/Y H:i');
        if ($max !== null) $minMax[] = 'max = '.$max->format('d/m/Y H:i');
        // build the info
        $info = implode(', ', $minMax);
        // has info?
        if ($info != '') $info = " ($info)";
        // demanem la data
        do
        {
            $value = static::getString($msg.$info, $canBeEmpty, $default instanceof DateTime ? $default->format('d/m/Y H:i') : null);
            // check for empty values
            if ($canBeEmpty and $value == '')
            {
                return null;
            }
            // print the message
            $value = DateTime::createFromFormat('d/m/Y H:i', $value);
            // comprovem que tenim una data valida
            if ( ! $value instanceof DateTime)
            {
                static::error("No has introduït una data vàlida.");
                continue;
            }
            // validem les dates
            if ($min !== null)
            {
                if (compareDates($value, $min) < 0)
                {
                    static::error("La data '".$value->format('d/m/Y H:i')."' és massa vella.");
                    continue;
                }
            }
            if ($max !== null)
            {
                if (compareDates($value, $max) > 0)
                {
                    static::error("La data '".$value->format('d/m/Y H:i')."' és massa futura.");
                    continue;
                }
            }
            return $value;
        }
        while (true);
    }

    /**
     * Mostra una barra amb percentatges.
     *
     * DataSet: [
     *   'value': float (0.0 - 1.0)
     *   'color': string (red, green, blue, etc.)
     * ]
     *
     * @param array $dataSet Els valors a mostrar.
     * @param string $char El caràcter ASCII a fer servir com a gràfic.
     * @param bool $displayPercentages TRUE = Mostra els %, FALSE = no mostra res.
     * @param int $length Longitud de la línia (per defecte 78).
     */
    public static function printChartPercentageBar(array $dataSet, string $char = '█', bool $displayPercentages = true, int $length = 78): void
    {
        $chart = '';
        // convert each % to screen value
        $values = array_map(function ($item) use ($length) { return (int)round($item['value']*100*$length/100, 0); }, $dataSet);
        // print percentages
        if ($displayPercentages)
        {
            foreach ($dataSet as $index => $item)
            {
                if ($values[$index] === 0) continue;
                // init color
                $chart .= '<'.$item['color'].'>';
                // open tag
                if ($values[$index] >= 2) $chart .= '┌';
                // print percentage
                if ($values[$index] > 9) $chart .= mb_str_pad(' '.round($item['value']*100, 2).'% ', $values[$index] - 2, '─', STR_PAD_BOTH);
                elseif ($values[$index] >= 3) $chart .= str_repeat('─', $values[$index] - 2);
                elseif ($values[$index] === 1) $chart .= '─';
                // close tag
                if ($values[$index] >= 2) $chart .= '┐';
            }
            $chart .= PHP_EOL;
        }
        // print percentage bar representation
        foreach ($dataSet as $index => $item)
        {
            $chart .= '<'.$item['color'].'>'.str_repeat($char, $values[$index]);
        }
        // print the chart
        static::line($chart);
    }
}
