<?php

namespace Beecubu\Foundation\Cli\Application;

class Application
{
    /** @var ViewController[] $views */
    protected $views = [];

    /**
     * @param ViewController $mainController La vista principal.
     */
    public function __construct(ViewController $mainController)
    {
        $this->pushView($mainController);
        $this->main();
    }

    /**
     * Obté la vista actual a renderitzar.
     *
     * @return ViewController
     */
    protected function currentView(): ViewController
    {
        return $this->views[count($this->views) - 1];
    }

    /**
     * El main loop de l'app.
     *
     * @return void
     */
    protected function main(): void
    {
        $exit = false;
        do
        {
            $currentView = $this->currentView();
            $currentView->view();
            // check if the current view in stack is finished
            if ($this->currentView()->finished())
            {
                if (count($this->views) === 1)
                {
                    $exit = true;
                }
                elseif ($currentView === $this->currentView())
                {
                    $this->back();
                }
            }
        }
        while ( ! $exit);
    }

    /**
     * Afegeix una nova vista al stack.
     *
     * @param ViewController $viewController El View a afegir.
     *
     * @return void
     */
    public function pushView(ViewController $viewController): void
    {
        $viewController->setApplication($this);
        // push this view
        $this->views[] = $viewController;
    }

    /**
     * Torna a la vista anterior.
     *
     * @return void
     */
    public function back(): void
    {
        if (count($this->views) > 1)
        {
            $view = array_pop($this->views);
            // remove the application
            $view->unsetApplication();
        }
    }

    /**
     * Torna a la primera vista.
     *
     * @return void
     */
    public function home(): void
    {
        $this->views = [$this->views[0]];
    }
}
