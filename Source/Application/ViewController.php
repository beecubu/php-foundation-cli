<?php

namespace Beecubu\Foundation\Cli\Application;

abstract class ViewController
{
    /** @var Application|null $application */
    protected $application;

    /**
     * Assigna l'aplicació actual.
     *
     * @param Application $application
     *
     * @return void
     */
    final public function setApplication(Application $application): void
    {
        $this->application = $application;
    }

    /**
     * Desconfigura l'aplicació actual.
     *
     * @return void
     */
    final public function unsetApplication(): void
    {
        $this->application = null;
    }

    /**
     * El punt d'inici del render del view.
     *
     * @return void
     */
    abstract public function view(): void;

    /**
     * Retorna si aquesta vista ha finalitzat.
     *
     * @return bool
     */
    abstract public function finished(): bool;
}
